/**
 * Script to find the shortest distance between two nodes.
 *
 * Should be run using node:
 *
 * node index.js [data file] [start node] [end node]
 *
 * @author Ben Gibbard <bgibbard@gmail.com>
 */

const fs = require('fs');
const Graph = require('./classes/graph.js');

// Parse the command line params
const args = process.argv.slice(2);

// Ensure that we have the required number of params
if (args.length !== 3) {
    console.warn('One or more command line parameters missing, please supply [filename] [origin node] [destination node]');
    process.exit(1);
}

const filename = args[0];
const origin = args[1];
const destination = args[2];

// Read the contents of the .dat file
// Catch & report exception thrown if file doesn't exist etc.
let data;
try {
    data = fs.readFileSync(filename).toString();
} catch (exception) {
    if (exception.errno === -4058) {
        console.warn('Unable to find node data file "' + filename + '", please check and try again.');
    } else {
        console.error(exception);
    }
    process.exit(1);
}

// Create Graph instance to store nodes and perform shortest path calculation
let graph;
try {
    graph = new Graph(data);
} catch (e) {
    console.error('Exception thrown creating Graph: ' + e);
    process.exit(1);
}
let path = graph.shortestPath(origin, destination);

console.log('');
if (path.distance === graph.infinity) {
    console.log('There is no path from node "' + origin + '" to node "' + destination + '".');
} else {
    for (let n in path.path) {
        console.log(path.path[n]);
    }
}
