/**
 * Class to store information on a node, ie, it's current name and the other nodes that it is linked to.
 */

module.exports = class Node {
    constructor(name) {
        this.name = name;
        this.linkDistances = {};
    }

    /**
     * Add a link from this node to another node. Records the name of the destination node and the distance to that node.
     *
     * @param   toNode      The name of the node we can link to.
     * @param   distance    The distance (integer) to the node.
     */
    addLink(toNode, distance) {
        if (!(toNode in this.linkDistances)) {
            this.linkDistances[toNode] = parseInt(distance);
        }
    }
};
