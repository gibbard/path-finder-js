/**
 * Graph class to store a graph's nodes, and provide functionality for finding the shortest
 * path between any two nodes.
 *
 * Uses Dijkstra's algorithm
 * https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
 */

const Node = require('./node.js');

module.exports = class Graph {
    constructor(data) {
        this.nodes = {};

        // "Constant" to allow us to identify when an end node cannot be reached
        this.infinity = "Infinity";

        // Load the node data
        this.#parseData(data);
    }

    /**
     * Load the node information from the supplied .dat file contents
     * @param data
     */
    #parseData(data) {
        // Regular expressions to validate a node row or the header (if supplied)
        let regexHeader = /^startNode\s+endNode\s+distance$/i;
        let regexNode = /^([0-9A-Z]+)\s+([0-9A-Z]+)\s+([0-9]+)$/i;

        // Create nodes and paths within the graph
        data = data.split('\n');
        for (let row = 0; row < data.length; row++) {
            if (data[row] === '') {
                continue;
            }

            // Node row?
            let nodeMatch = regexNode.exec(data[row]);
            if (nodeMatch !== null) {
                let node = this.#getNode(nodeMatch[1]);
                node.addLink(nodeMatch[2], parseInt(nodeMatch[3]));
            } else {
                // Header row?
                let headerMatch = row === 0 ? regexHeader.exec(data[row]) : null;
                if (headerMatch === null) {
                    throw new Error('Row "' + data + '" is incorrectly formatted, aborting.');
                }
            }
        }
    }

    /**
     * Get a node from the graph by it's name.
     * If the node doesn't already exist it will be created.
     *
     * @param  {String}     name    The name of the node.
     *
     * @return {Node}               The Node object for the supplied node details.
     */
    #getNode(name) {
        if (!(name in this.nodes)) {
            this.nodes[name] = new Node(name);
        }
        return this.nodes[name];
    }

    /**
     * Get the node that has the shortest overall distance from the start node, and that we
     * have not visited yet. This is the next node to search from.
     *
     * @param   {object}    distances   Array of nodes and their currently known distances from the start point.
     * @param   {array}     visited     Array of node names that we have visited so far.
     *
     * @return  {String}                Name of the node with the shortest distance from the start node, which we have not yet visited.
     */
    #shortestDistanceUnvisitedNode(distances, visited) {
        let shortestNode = null;
        let shortestDistance = null;

        for (let node in distances) {
            if (distances[node] === this.infinity) {
                continue;
            }
            if ((shortestDistance === null || distances[node] < shortestDistance) && !visited.includes(node)) {
                shortestNode = node;
                shortestDistance = distances[node];
            }
        }

        return shortestNode;
    }

    /**
     * Dijkstras path finding algorithm, ideal when no heuristics to tune the A-Star algorithm.
     *
     * https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
     *
     * @param   start   The name of the start node.
     * @param   end     The name of the end node.
     *
     * @return          Array of node names to get from the start node to the end node in the shortest distance.
     */
    shortestPath(start, end) {
        // Record the overall distance to a node from the start node that we have discovered so far.
        let distances = {}
        distances[end] = this.infinity;

        // Record the parent node for a child node as we create a path
        let parents = {};
        parents[end] = null;

        // Populate distances & parent for nodes linked from the starting node
        if (start in this.nodes) {
            for (let linked in this.nodes[start].linkDistances) {
                distances[linked] = this.nodes[start].linkDistances[linked];
                parents[linked] = start;
            }
        }

        // Record visited nodes
        let visited = [];

        // Find the current node with shortest overall distance from the start node so far
        let node = this.#shortestDistanceUnvisitedNode(distances, visited);

        // And keep traversing from that node to child nodes until we either find the end node, or
        // discover that there is no path from start to end.
        while (node) {
            // Distance from the start node so far for this current node
            let distance = distances[node];

            // Process the child nodes for this current node
            if (node in this.nodes) {
                for (let child in this.nodes[node].linkDistances) {
                    // Skip if this is the start node
                    if (child === start) {
                        continue;
                    }

                    // Save the distance from the start node to this child node
                    let childDistance = distance + this.nodes[node].linkDistances[child];

                    // If there's no distance from the start node to this child node,
                    // or if the distance is shorter than the existing distance from
                    // the start node to the child node, then update known distances
                    // and set the parent node
                    if (!(child in distances) || distances[child] > childDistance || distances[child] === this.infinity) {
                        // Distance to this child node from the start node
                        distances[child] = childDistance;

                        // Add to parents so that we can determine our shortest path later
                        parents[child] = node;
                    }
                }
            }

            // Record the current node in the visited array so that we don't re-visit later
            visited.push(node);

            // Find the current node that has the shortest overall distance from the start
            node = this.#shortestDistanceUnvisitedNode(distances, visited);
        }

        // We have either reached the end node, or no route from start -> end could be found
        let shortestPath = [];
        if (distances[end] !== this.infinity) {
            // We have found a route. Populate the shortest path array
            shortestPath.push(end);
            let parent = parents[end];
            while (parent) {
                shortestPath.push(parent);
                parent = parents[parent];
            }

            // Reverse path so that it is from start -> end
            shortestPath.reverse();
        }

        // Return the shortest path & the end node's distance from the start node
        return {
            distance: distances[end],
            path: shortestPath
        }
    }
};
