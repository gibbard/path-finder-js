/**
 * Unit tests to verify the Graph path finding functionality.
 */

const assert = require('assert');
const Graph = require('./../classes/graph.js');

describe('Graph class unit testing', () => {
    it('test_bad_data', () => {
        // Empty row just ignored
        assert.doesNotThrow(function () { new Graph(''); }, Error);

        // Shouldn't throw an exception if OK
        assert.doesNotThrow(function () { new Graph('A B 10'); }, Error);
        assert.doesNotThrow(function () { new Graph('A    B     10'); }, Error);
        assert.doesNotThrow(function () { new Graph('A B 10\nB A 10'); }, Error);
        assert.doesNotThrow(function () { new Graph('A B 10\nB A 10\n'); }, Error);
        assert.doesNotThrow(function () { new Graph('startNode endNode distance\nA B 10\n'); }, Error);
        assert.doesNotThrow(function () { new Graph('startNode    endNode     distance\nA B 10\n'); }, Error);

        // Bad data does throw an exception
        assert.throws(function () { new Graph('A')}, Error);
        assert.throws(function () { new Graph('A B')}, Error);
        assert.throws(function () { new Graph('A B C')}, Error);
        assert.throws(function () { new Graph('A B 1 C')}, Error);
    });

    it('test_two_nodes_bidirectional', () => {
        let graph = new Graph('A B 10\nB A 10');
        let path = graph.shortestPath('A', 'B');
        assert.equal(path['distance'], 10);
        assert.deepEqual(path['path'], ['A', 'B']);
        path = graph.shortestPath('B', 'A');
        assert.equal(path['distance'], 10);
        assert.deepEqual(path['path'], ['B', 'A']);
    });

    it('test_two_nodes_unidirectional', () => {
        let graph = new Graph('A B 10');
        let path = graph.shortestPath('A', 'B');
        assert.equal(path['distance'], 10);
        assert.deepEqual(path['path'], ['A', 'B']);
        path = graph.shortestPath('B', 'A');
        assert.equal(path['distance'], graph.infinity);
        assert.deepEqual(path['path'], []);
    });

    it('test_three_nodes_bidirectional_single_path', () => {
        let graph = new Graph('A B 10\nB A 10\nB C 20\nC B 20');
        let path = graph.shortestPath('A', 'B');
        assert.equal(path['distance'], 10);
        assert.deepEqual(path['path'], ['A', 'B']);
        path = graph.shortestPath('B', 'C');
        assert.equal(path['distance'], 20);
        assert.deepEqual(path['path'], ['B', 'C']);
        path = graph.shortestPath('A', 'C');
        assert.equal(path['distance'], 30);
        assert.deepEqual(path['path'], ['A', 'B', 'C']);

        path = graph.shortestPath('C', 'A');
        assert.equal(path['distance'], 30);
        assert.deepEqual(path['path'], ['C', 'B', 'A']);
        path = graph.shortestPath('C', 'B');
        assert.equal(path['distance'], 20);
        assert.deepEqual(path['path'], ['C', 'B']);
        path = graph.shortestPath('B', 'A');
        assert.equal(path['distance'], 10);
        assert.deepEqual(path['path'], ['B', 'A']);
    });

    it('test_three_nodes_unidirectional_single_path', () => {
        let graph = new Graph('A B 10\nB C 20');
        let path = graph.shortestPath('A', 'B');
        assert.equal(path['distance'], 10);
        assert.deepEqual(path['path'], ['A', 'B']);
        path = graph.shortestPath('B', 'C');
        assert.equal(path['distance'], 20);
        assert.deepEqual(path['path'], ['B', 'C']);
        path = graph.shortestPath('A', 'C');
        assert.equal(path['distance'], 30);
        assert.deepEqual(path['path'], ['A', 'B', 'C']);

        path = graph.shortestPath('C', 'A');
        assert.equal(path['distance'], graph.infinity);
        assert.deepEqual(path['path'], []);
        path = graph.shortestPath('C', 'B');
        assert.equal(path['distance'], graph.infinity);
        assert.deepEqual(path['path'], []);
        path = graph.shortestPath('B', 'A');
        assert.equal(path['distance'], graph.infinity);
        assert.deepEqual(path['path'], []);
    });

    it('test_three_nodes_split_path', () => {
        // Test single step being either shorter or longer than double step, ensure path chosen correct for either
        // condition
        for (let distance = 1; distance <= 40; distance++) {
            let graph = new Graph('A B 10\nB A 10\nB C 20\nC B 20\nA C ' + distance + '\nC A ' + distance);
            let path = graph.shortestPath('A', 'C')
            if (distance < 30) {
                assert.equal(path['distance'], distance);
                assert.deepEqual(path['path'], ['A', 'C']);
            } else if (distance === 30) {
                assert.equal(path['distance'], distance);
            } else {
                assert.equal(path['distance'], 30);
                assert.deepEqual(path['path'], ['A', 'B', 'C']);
            }
        }
    });

    it('test_two_paths_choice_returns_shortest', () => {
        // Given a choice of two routes, ensure we are always given the shortest
        // Route is A->B->D or A->C->D
        // "Dead-end" of B->X should have no impact
        for (let distance_a_b = 1; distance_a_b <= 10; distance_a_b++) {
            for (let distance_b_d = 1; distance_b_d <= 10; distance_b_d++) {
                for (let distance_b_x = 1; distance_b_x <= 10; distance_b_x++) {
                    for (let distance_a_c = 1; distance_a_c <= 10; distance_a_c++) {
                        for (let distance_c_d = 1; distance_c_d <= 10; distance_c_d++) {
                            let graph = new Graph(
                                'A B ' + distance_a_b + '\n'
                                + 'B A ' + distance_a_b + '\n'
                                + 'B D ' + distance_b_d + '\n'
                                + 'D B ' + distance_b_d + '\n'
                                + 'B X ' + distance_b_x + '\n'
                                + 'X B ' + distance_b_x + '\n'
                                + 'A C ' + distance_a_c + '\n'
                                + 'C A ' + distance_a_c + '\n'
                                + 'C D ' + distance_c_d + '\n'
                                + 'D C ' + distance_c_d
                            );

                            // A -> D
                            let path = graph.shortestPath('A', 'D')
                            let a_b_d = distance_a_b + distance_b_d
                            let a_c_d = distance_a_c + distance_c_d
                            if (a_b_d < a_c_d) {
                                assert.equal(path['distance'], a_b_d)
                                assert.deepEqual(path['path'], ['A', 'B', 'D'])
                            } else if (a_c_d < a_b_d) {
                                assert.equal(path['distance'], a_c_d)
                                assert.deepEqual(path['path'], ['A', 'C', 'D'])
                            } else {
                                assert.equal(path['distance'], a_c_d)
                            }

                            // And how about A -> B (but should go A -> C -> D -> B when shorter)
                            path = graph.shortestPath('A', 'B')
                            let a_b = distance_a_b
                            let a_c_d_b = distance_a_c + distance_c_d + distance_b_d
                            if (a_b < a_c_d_b) {
                                assert.equal(path['distance'], a_b)
                                assert.deepEqual(path['path'], ['A', 'B'])
                            } else if (a_c_d_b < a_b) {
                                assert.equal(path['distance'], a_c_d_b)
                                assert.deepEqual(path['path'], ['A', 'C', 'D', 'B'])
                            } else {
                                assert.equal(path['distance'], a_c_d_b)
                            }
                        }
                    }
                }
            }
        }
    })
        // Potentially Long running test!
        .timeout(10000);
});
