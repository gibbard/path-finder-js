
# path-finder-js

Script to find the shortest route between two nodes using Dijkstra's algorithm.
* https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

# Fetching
Clone the latest version of this project:
```
> git clone https://gitlab.com/gibbard/path-finder-js
```

And then run npm install in the path-finder-js folder
```
> npm install
```

# Using
This script can be executed using node v16 within the path-finder-js folder:
````
> node index.js [data file] [start node] [end node]
````

The output will be a line-separated list of nodes that represent the shortest
path between [start node] and [end node], or a warning if no path can be found.

For example - A valid path:
```
> node index.js exmouth-links.dat J1053 J1037

J1053
J1035
J1036
J1037
```
No path:
```
> node index.js exmouth-links.dat J1001 J1023

There is no path from node "J1001" to node "J1023".
```

# Testing
Unit testing using mocha can be performed by running:
```
> mocha
```
within the path-finder-js folder. No sample data is required for unit testing.

If mocha package not installed:
```
> sudo npm install mocha -g
```

Unit testing has been verified on Node v16.13.0 (Windows & Ubuntu).

# Sample data
A sample road network (exmouth-links.dat) is provided here:

https://gist.github.com/byrney/a64f62850d1c5f5ca287ce14dec5f7ad

The format of the file is

```
startNode  endNode  distance
A          B        2
B          C        4
```
